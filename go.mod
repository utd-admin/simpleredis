module github.com/xyproto/simpleredis

go 1.8

require (
	github.com/gomodule/redigo v1.8.8
	github.com/xyproto/pinterface v1.5.3
)
